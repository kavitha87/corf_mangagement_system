//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CORF_MGMT
{
    using System;
    using System.Collections.Generic;
    
    public partial class recipe
    {
        public int Re_Id { get; set; }
        public int Rawmeterial_Rm_Id { get; set; }
        public int CookedFood_Cf_Id { get; set; }
        public decimal Quantity { get; set; }
    
        public virtual cookedfood cookedfood { get; set; }
        public virtual rawmaterial rawmaterial { get; set; }
    }
}
