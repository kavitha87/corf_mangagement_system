﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity;


namespace CORF_MGMT.Controllers
{
    public class StudyController : Controller
    {
        private ANKEntities an = new ANKEntities();
        // GET: Study
        public ActionResult Study()
        {
            var studies = an.studies.Include(s => s.sponsor);
            return View(studies.ToList());
        }
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult create([Bind(Include = "Study_Id,Study_Name,Start_Date,End_Date,Study_Duration,Population_Type,No_Of_Visit,Sponsor_Id,Objective")] study study)
        {

            if (ModelState.IsValid)
            {
                an.studies.Add(study);
                an.SaveChanges();
                return RedirectToAction("Study");
            }

            ViewBag.Sponsor_Id = new SelectList(an.sponsors, "Sponsor_Id", "Sponsor_Name", study.Sponsor_Id);
            return View(study);
        }
    }
}
