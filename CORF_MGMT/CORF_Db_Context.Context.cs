﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CORF_MGMT
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class ANKEntities : DbContext
    {
        public ANKEntities()
            : base("name=ANKEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<consumption> consumptions { get; set; }
        public virtual DbSet<cookedfood> cookedfoods { get; set; }
        public virtual DbSet<nutri_value> nutri_value { get; set; }
        public virtual DbSet<nutrient> nutrients { get; set; }
        public virtual DbSet<rawmaterial> rawmaterials { get; set; }
        public virtual DbSet<recipe> recipes { get; set; }
        public virtual DbSet<sponsor> sponsors { get; set; }
        public virtual DbSet<study> studies { get; set; }
        public virtual DbSet<subject> subjects { get; set; }
        public virtual DbSet<user> users { get; set; }
    }
}
